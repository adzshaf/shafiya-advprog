package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import org.springframework.stereotype.Service;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;

import java.util.List;
import java.util.Optional;

@Service
public class SoulServiceImpl implements SoulService {

    private final SoulRepository soulRepository;

    public SoulServiceImpl(SoulRepository soulRepository) {
        this.soulRepository = soulRepository;
    }

    @Override
    public List<Soul> findAll() {
        return soulRepository.findAll();
    }

    @Override
    public Optional<Soul> findSoul(Long id) {
        return soulRepository.findById(id);
    }

    @Override
    public Soul rewrite(Soul soul) {
        return soulRepository.save(soul);
    }

    @Override
    public void erase(Long id) {
        soulRepository.deleteById(id);
    }

    @Override
    public Soul register(Soul soul) {
        return soulRepository.save(soul);
    }
}
