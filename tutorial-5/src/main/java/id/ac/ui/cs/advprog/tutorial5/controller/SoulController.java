package id.ac.ui.cs.advprog.tutorial5.controller;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
    @RequestMapping(path = "/soul")
    public class SoulController {

        @Autowired
        private SoulService soulService;

        @GetMapping
        public ResponseEntity<List<Soul>> findAll() {
            List<Soul> listSoul = soulService.findAll();
            return new ResponseEntity<>(listSoul, HttpStatus.OK);
        }

        @PostMapping
        public ResponseEntity<Soul> create(@RequestBody Soul soul) {
            Soul newSoul = soulService.register(soul);
            return new ResponseEntity<>(newSoul, HttpStatus.CREATED);
        }

        @GetMapping("/{id}")
        public ResponseEntity<Soul> findById(@PathVariable Long id) {
            if(!soulService.findSoul(id).isPresent()) {
                throw new NoSuchElementException("Soul not found!");
            }else {
                return new ResponseEntity<>(soulService.findSoul(id).get(), HttpStatus.OK);
            }
        }

        @PutMapping("/{id}")
        public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul) {
            Soul updatedSoul = soulService.rewrite(soul);
            return new ResponseEntity<>(updatedSoul, HttpStatus.OK);
        }

        @DeleteMapping("/{id}")
        public ResponseEntity delete(@PathVariable Long id) {
            soulService.erase(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }
