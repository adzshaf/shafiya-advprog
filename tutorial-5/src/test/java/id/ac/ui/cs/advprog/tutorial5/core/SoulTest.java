package id.ac.ui.cs.advprog.tutorial5.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class SoulTest {

    private Class<?> soulClass;

    private Soul soul;

    @BeforeEach
    public void setUp() throws Exception {
        soul = new Soul();
        soulClass = Class.forName("id.ac.ui.cs.advprog.tutorial5.core.Soul");
    }

    @Test
    public void testSoulIsConcreteClass() {
        assertFalse(Modifier.isAbstract(soulClass.getModifiers()));
    }

    @Test
    public void testSoulGetId() throws NoSuchMethodException, SecurityException {
        Method getId = soulClass.getDeclaredMethod("getId");

        assertEquals("long",
                getId.getGenericReturnType().getTypeName());
        assertEquals(0, getId.getParameterCount());
    }

    @Test
    public void testSoulGetName() throws NoSuchMethodException, SecurityException {
        Method getName = soulClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void testSoulGetAge() throws NoSuchMethodException, SecurityException {
        Method getAge = soulClass.getDeclaredMethod("getAge");

        assertEquals("int",
                getAge.getGenericReturnType().getTypeName());
        assertEquals(0, getAge.getParameterCount());
    }

    @Test
    public void testSoulGetGender() throws NoSuchMethodException, SecurityException {
        Method getGender = soulClass.getDeclaredMethod("getGender");

        assertEquals("java.lang.String",
                getGender.getGenericReturnType().getTypeName());
        assertEquals(0, getGender.getParameterCount());
    }

    @Test
    public void testSoulGetOccupation() throws NoSuchMethodException, SecurityException {
        Method getOccupation = soulClass.getDeclaredMethod("getOccupation");

        assertEquals("java.lang.String",
                getOccupation.getGenericReturnType().getTypeName());
        assertEquals(0, getOccupation.getParameterCount());
    }

    @Test
    public void testGetId() {
        long id = soul.getId();

        assertEquals(0, id);
    }

    @Test
    public void testGetName() {
        String name = "Shafiya";
        soul.setName(name);

        assertEquals(name, soul.getName());
    }

    @Test
    public void testGetAge() {
        int age = 23;
        soul.setAge(age);

        assertEquals(age, soul.getAge());
    }

    @Test
    public void testGetGender() {
        String gender = "F";
        soul.setGender(gender);

        assertEquals(gender, soul.getGender());
    }

    @Test
    public void testGetOccupation() {
        String occupation = "Kuli kompes";
        soul.setOccupation(occupation);

        assertEquals(occupation, soul.getOccupation());
    }

}

