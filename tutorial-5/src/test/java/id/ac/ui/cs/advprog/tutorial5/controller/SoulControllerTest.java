package id.ac.ui.cs.advprog.tutorial5.controller;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.MultiValueMap;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import id.ac.ui.cs.advprog.tutorial5.service.SoulServiceImpl;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.HashMap;
import java.util.Map;

@WebMvcTest(controllers = SoulController.class)
public class SoulControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SoulServiceImpl soulService;

    @Test
    public void whenSoulHomeURIIsAccessedItShouldHandledByFindAll() throws Exception {
        mockMvc.perform(get("/soul/"))
                .andExpect(status().isOk())
                .andExpect(handler()
                        .methodName("findAll"));
    }
}

