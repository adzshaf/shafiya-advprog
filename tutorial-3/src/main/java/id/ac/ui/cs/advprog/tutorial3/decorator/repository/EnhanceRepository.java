package id.ac.ui.cs.advprog.tutorial3.decorator.repository;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer.EnhancerDecorator;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EnhanceRepository {

    public void enhanceToAllWeapons(ArrayList<Weapon> weapons){

        for (Weapon weapon: weapons) {
            switch (weapon.getName()) {
                case "Gun":
                    Weapon gun;
                    gun = WeaponProducer.WEAPON_GUN.createWeaponEnhancer();
                    gun = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(gun);
                    gun = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(gun);
                    int index = weapons.indexOf(weapon);
                    weapons.set(index,gun);
                    break;

                //TODO: Complete me
            }
        }
    }
}
