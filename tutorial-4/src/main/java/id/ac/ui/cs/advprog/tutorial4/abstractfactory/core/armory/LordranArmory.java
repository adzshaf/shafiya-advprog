package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.ShiningArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ShiningForce;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ShiningBuster;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;

public class LordranArmory implements Armory {

    @Override
    public Armor craftArmor() {
        // TODO fix me
        return null;
    }

    @Override
    public Weapon craftWeapon() {
        // TODO fix me
        return null;
    }

    @Override
    public Skill learnSkill() {
        // TODO fix me
        return null;
    }
}
